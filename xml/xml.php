<?php

// load the XML file
$spots   = simplexml_load_file('spots.xml');

// get all song elements as an array
$options = iterator_to_array($spots->spot, false);

// output json
echo json_encode($options);