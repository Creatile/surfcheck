<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Prévisions météo des spots de surf et kitesurf en France">
    <meta name="author" content="">
    <title>Météo des spots de surf et kitesurf</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Specific style CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Header -->
    <header id="top" class="header">
        <div class="text-vertical-center container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center ">
                   <h1>Check la météo du spot</h1>
                    <div class="form-group searchcode">
                    <h2>Prévisions surf et kitesurf</h2>
                    <form id="search" method="" action="">
                    <label>Sélectionne un spot!</label>
                    <small>Données fournies par World Weather online <a title="Open Street Map" href="https://www.worldweatheronline.com">worldweatheronline.com</a></small>
                    <select class="form-control" name="x" id="option"></select>
                    <button id="findCode" type="submit" class="btn btn-success btn-lg">Rechercher</button>
                    <input type="hidden" class="form-control" id="ville">
                    </form>
                    <div class="alert alert-success" id="result"></div>
                    <div class="alert alert-danger" id="error"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </header>
    <!-- About -->
    <section id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Check la météo des spots</h2>

                    <p class="lead">Météo du surf et du kitesurf en France</p>
                    <p><small>Service gratuit de recherche</small></p>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <ul class="list-inline">
                        <li>
                            <a href="#"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dribbble fa-fw fa-3x"></i></a>
                        </li>
                    </ul>
                    <hr class="small">
                    <p class="text-muted">Copyright &copy; surfcheck 2016</p>
                </div>
            </div>
        </div>
    </footer>
<script src="js/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script>
	$.getJSON("xml/xml.php",function(j){
       var $option = $("#option");
          $option.empty();
          $.each(j, function () {
            $option.append($('<option data-wkt="'+this.wkt+'"></option>').attr("value", this.id).text(this.name));
          });   
});
		$('select#option').on('change', function() {
        var spot_wkt = $('select#option option:selected').attr('data-wkt');
      $('#ville').val(spot_wkt);
      $('#findCode').trigger('click');
      //console.log('product id is ' + prod_id);
    });
    
    
	/*$('#findCode').keypress(function (e) {
  if (e.which == 13) {
    $('form#search').submit();
    return false;    //<---- Add this line
  }
});*/
	$('#findCode').click(function(event){
		$('.alert').hide();
		var result=0;
		event.preventDefault();
		$.ajax({
			type: "GET",
			url: "http://api.worldweatheronline.com/free/v2/marine.ashx?key=5ea445492c28853839f1d820f0668&format=xml&q="+encodeURIComponent($('#ville').val())+"&lang=fr&tide=yes",
			datatype:"xml",
			success: processXML
		});
		function processXML(xml){
			//console.log(xml);
			$(xml).find("weather").each(function(){
				var results = "";
				$(xml).find("hourly").each(function(i,v){
				if(v){
					console.log(v);
					results += '<tr>';
					results += '<td>'+$(v).find('time').text()+'</td>';
					results += '<td>'+$(v).find('swellHeight_m').text()+' m</td>';
					results += '<td>'+$(v).find('swellPeriod_secs').text()+' S</td>';
					results += '<td>'+$(v).find('windspeedKmph').text()+' Km/h</td>';
					results += '<td>'+$(v).find('winddir16Point').text()+'</td>';
					results += '<td><img src="'+$(v).find('weatherIconUrl').text()+'"></td>';
					results += '</tr>';
				}else{
					results += '';
				}
				});
				if(results){
					$('#result').html("Prévisions de surf : <table><thead><tr><th>Heure</th><th>Houle</th><th>Période</th><th>Vent</th><th>Dir. vent</th><th>Temps</th></tr></thead><tbody>"+results+"</tbody></table>").fadeIn();
				}else{
					$('#error').html("Aucun résultat pour cette recherche.").fadeIn();
				}
			});
		}
	});
</script>
</body>
</html>